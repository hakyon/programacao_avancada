#ifndef PILHA_H
#define PILHA_H

#include <iostream>
#include <string>

using namespace::std;


typedef struct {
  int topo;
  string item ;
}PILHA;

class Pilha
{
public:

    Pilha(int tamanho = 100){}
    Pilha(int tamanho) : tamanho(tamanho) {}

    void imprimir();
    int Tamanho();
    int Empilhar(PILHA &p,string nome);
    void Desempilhar(PILHA &p);
    bool PilhaVazia(PILHA &p);
    bool PilhaCheia(PILHA &p);

private:

};

#endif // PILHA_H
