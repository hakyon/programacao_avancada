#include <iostream>

#include "singleton.h"

using namespace std;

int main()
{
    //new Singleton(); // Won't work
    Singleton* s = Singleton::getInstance();
    Singleton* r = Singleton::getInstance();

    /*Os endereço devem aparecer iguais. */
    cout << s << endl;
    cout << r << endl;

    return 0;
}
