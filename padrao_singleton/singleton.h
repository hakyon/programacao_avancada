#ifndef SINGLETON_H
#define SINGLETON_H


class Singleton
{

private:
    /* Aqui nos vamos armazenar a instancia. */
    static Singleton* instance;

    /* construtor privado mantem em uma unica instancia */
    Singleton();

public:
    /* Método de acesso */
    static Singleton* getInstance();
};

#endif // SINGLETON_H
