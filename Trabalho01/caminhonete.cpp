#include "veiculo.h"

void Caminhonete::ImprimaDadosVeiculo(){

    cout << "Cor:" << this->cor << endl;
    cout << "Modelo:" << this->modelo << "Toneladas"<< endl;
    cout << "Capacidade:" << this->capacidade << "Toneladas"<< endl;
    cout << "Comprimento:" << this->comprimento <<"metros"<< endl;
    cout << "Altura maxima:" << this->altura_max <<"metros"<< endl;
    cout << "Placa:" << this->placa << endl;
    cout << "Peso:" << this->peso <<"Kg"<< endl;
    cout << "Velocidade maxima:" << this->velocidade_max <<"Km/h"<<endl;
    cout << "Preço" << this->preco <<"R$"<<endl;
}
