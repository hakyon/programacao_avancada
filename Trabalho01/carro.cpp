#include "veiculo.h"

void Carro::ImprimaDadosVeiculo(){

    cout << "Modelo:" << modelo << endl;
    cout << "Cor:" << cor << endl;
    cout << "Placa:" << this->placa << endl;
    cout << "Peso:" << this->peso <<"Kg"<<endl;
    cout << "Velocidade maxima:" << this->velocidade_max <<"Km/h"<< endl;
    cout << "Preco" << this->preco <<"R$"<< endl;

}
