#include "veiculo.h"

void Veiculo::ImprimaDadosVeiculo()
{
    cout << "Placa:" << this->placa << endl;
    cout << "Peso:" << this->peso << "Kg" << endl;
    cout << "Velocidade maxima:" << this->velocidade_max << "Km/h" << endl;
    cout << "Preço" << this->preco << "R$" << endl;

}
