#ifndef VEICULO_H
#define VEICULO_H

#include <iostream>
#include <string>
#include <string.h>

using namespace std;

class Veiculo
{
private:
    string placa;
    float peso;
    float velocidade_max;
    float preco;

public:
    Veiculo(){
        this->placa ="0000-000";
        this->peso = 0.0;
        this->velocidade_max = 0.0;
        this->preco = 0.0;
    }
    Veiculo(string placa,float peso, float velocidade, float preco) :
       placa(placa),peso(peso), velocidade_max(velocidade), preco(preco)
    {
        this->placa = placa;
        this->peso = peso;
        this->velocidade_max = velocidade;
        this->preco = preco;
    }

void setPlaca(string p) { this->placa = p;}
string getPlaca() {return this->placa;}
void setPeso(float a) { this->peso = a;}
float getPeso() { return this->peso;}
void setVelocidadeMax(float b) { this->velocidade_max = b;}
float getVelocidadeMax(){return this->velocidade_max;}
void setPreco(float c) { this->preco = c;}
float getPreco(){return this->preco;}
virtual void ImprimaDadosVeiculo() = 0;


};

class Carro : public Veiculo
{
private:
    string modelo;
    string cor;
public:
    Carro(){
        modelo = "Default";
        cor = "Cinza";
    }
    Carro(string modelo,string cor,string placa,float peso,float velocidade,float preco) :
        Veiculo(placa,peso,velocidade,preco)
    {
        this->modelo = modelo;
        this->cor = cor;
    }

void setModelo(string a){modelo = a;}
string getModelo() {return this->modelo;}
void setCor(string b){cor = b;}
string getCor() {return this->cor;}
void ImprimaDadosVeiculo();

};

class Caminhao : public Carro
{
private:
    float capacidade; //em toneladas
    float comprimento; // em metros
    float altura_max; // em metros
public:
    Caminhao(){
        capacidade=0.0;
        comprimento=0.0;
        altura_max=0.0;
    }

    Caminhao(string cor, string modelo ,float capacidade, float comprimento,float altura_max,
             string placa,float peso, float velocidade, float preco) :
                        Carro(modelo,cor,placa,peso,velocidade,preco)

    {
        this->capacidade = capacidade;
        this->comprimento = comprimento;
        this->altura_max = altura_max;
    }


void setCapacidade(float a2) { capacidade = a2;}
float getCapacidade() { return this->capacidade;}
void setComprimento(float a3) {comprimento = a3;}
float getComprimento()	{return this->comprimento;}
void setAlturaMax(float a4) { altura_max = a4;}
float getAlturaMAx() { return this->altura_max;}
void ImprimaDadosVeiculo();

};


class Caminhonete : public Caminhao
{
private:


public:
    Caminhonete(string placa, float peso, float velocidade, float preco,
                string cor,string modelo , float capacidade , float comprimento , float altura_max) :
                Caminhao(cor,modelo,capacidade,comprimento,altura_max,placa,peso,velocidade,preco)
                {}



void ImprimaDadosVeiculo();

};



#endif // VEICULO_H
