TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    veiculo.cpp \
    carro.cpp \
    caminhao.cpp \
    caminhonete.cpp

HEADERS += \
    veiculo.h
