#include "conta.h"



void Conta::Extrato()
{
    cout << "Extrato de " + this->cliente_n.getNome() << endl;
    string name;
    name = "extratos/extrato" + to_string(this->numero) + ".txt";

    ifstream f(name.c_str());
    if (f.is_open()){
        cout << f.rdbuf();
    }
    cout << "Saldo Total : R$" << setprecision(4) << this->saldo << '\n' << '\n';
    return;
}

void Conta::Transferencia(Conta *Destino,float valor)
{
    string dia = to_string(day);
    string mes = to_string(month);
    string ano = to_string(year);
    string h = to_string(hour);
    string m = to_string(min);
    string v = to_string(valor);
    string c = to_string(Destino->numero);
    string linha =dia+"/"+mes+"/"+ano+"  " + h+":"+ m +"  Transferido para conta" + c + " valor :"+ v +"\n";

    Destino->saldo+=valor;
    this->saldo-=valor;
    string name;
    name = "extratos/extrato" + to_string(this->numero) + ".txt"; // aqui consigo garantir que cada extrato gerado seja referente a uma conta
    arquivo = fopen(name.c_str(), "a+");
    if (arquivo!=NULL)
    {
        fputs (linha.c_str(),arquivo);
        fclose (arquivo);
    }
    return ;

}

void Conta::Deposita(float valor)
{
    string dia = to_string(day);
    string mes = to_string(month);
    string ano = to_string(year);
    string h = to_string(hour);
    string m = to_string(min);
    string v = to_string(valor);
    string linha =dia+"/"+mes+"/"+ano+"  " + h+":"+ m +"  Depositado valor :"+ v +"\n";

    this->saldo+=valor;
    string name;
    name = "extratos/extrato" + to_string(this->numero) + ".txt";
    arquivo = fopen(name.c_str(), "a+");
    if (arquivo!=NULL)
    {
        fputs (linha.c_str(),arquivo);
        fclose (arquivo);
    }
    return;
}
void Conta::Retirada(float valor)
{
    string dia = to_string(day);
    string mes = to_string(month);
    string ano = to_string(year);
    string h = to_string(hour);
    string m = to_string(min);
    string v = to_string(valor);
    //v << setw(4) << valor ;
    string linha =dia+"/"+mes+"/"+ano+"  " + h+":"+ m +"  Retirado valor :"+ v +"\n";

    this->saldo-=valor;
    string name;
    name = "extratos/extrato" + to_string(this->numero) + ".txt";
    arquivo = fopen(name.c_str(), "a+");
    if (arquivo!=NULL)
    {
        fputs (linha.c_str(),arquivo);
        fclose (arquivo);
    }
    return;
}

void Conta::LimparExtrato()
{
    string name;
    name = "extratos/extrato" + to_string(this->numero) + ".txt";
    std::ofstream ofs (name, std::ios::out | std::ios::trunc);
    ofs.close ();
    return;
}


void ContaCorrente::AplicacaoJurosDiarios(int dias)
{
    this->saldo = this->saldo + ((0.01 * this->saldo ) * dias);
    return;
}

void ContaPoupanca::AplicacaoJurosDiarios(int dias)
{
    this->saldo = this->saldo + ((0.08 * this->saldo ) * dias);
    return;
}
