#ifndef CONTA_H
#define CONTA_H

#include <iostream>
#include <fstream>
#include <time.h>
#include <iomanip>

#include "cliente.h"

using namespace std;

class Conta
{        

protected:
    int numero ;
    Cliente cliente_n;
    float saldo;
    FILE *arquivo;

    time_t theTime;
    struct tm *aTime;
    int day;
    int month;
    int year;
    int hour;
    int min;

public:
    Conta(int n1,Cliente cliente_n) : numero(n1), cliente_n(cliente_n), saldo(0.0) , arquivo(NULL) {
        theTime = time(NULL);
        aTime = localtime(&theTime);
        day = aTime->tm_mday;
        month = aTime->tm_mon + 1;
        year = aTime->tm_year + 1900;
        hour = aTime->tm_hour;
        min = aTime->tm_min;

    }
    void Deposita(float valor);
    void Retirada(float valor);
    void Transferencia(Conta *ContaDestino, float valor);
    void Extrato();
    virtual void AplicacaoJurosDiarios(int dias) = 0;
    void LimparExtrato();

};

class ContaCorrente : public Conta
{
private:
    int n2;

public:
    ContaCorrente(int n1,Cliente c1) : Conta(n1,c1) {
        cout << "Criando conta Corrente :" << n1 <<" do cliente:" << c1.getNome() << endl;
    }
    void AplicacaoJurosDiarios(int dias);
};

class ContaPoupanca : public Conta
{
private:
    int n1;

public:
    ContaPoupanca(int n2,Cliente c2) : Conta(n2,c2) {
        cout << "Criando conta Poupanca :" << n2 <<" do cliente:" << c2.getNome() << endl;
    }
    void AplicacaoJurosDiarios(int dias);
};


#endif // CONTA_H
