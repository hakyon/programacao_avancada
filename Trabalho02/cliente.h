#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>

using namespace std;

class Cliente
{
private:

    string nome;

public:
    Cliente(string n = "") : nome(n){}
    string getNome(){return this->nome;}
    void  setNome(string n) {this->nome = n;}
};

#endif // CLIENTE_H
