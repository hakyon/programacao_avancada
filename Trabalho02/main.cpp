#include "conta.h"
#include "cliente.h"

using namespace std;

int main()
{

    Cliente cliente1("Joao");
    ContaCorrente *conta_corrente1 = new ContaCorrente(4859,cliente1);

    Cliente cliente2("Goku");
    ContaCorrente *conta_corrente2 = new ContaCorrente(8713,cliente2);

    Cliente cliente3("Bruno Borges");
    ContaPoupanca *conta_poupanca1 = new ContaPoupanca(71571,cliente3);

    Cliente cliente4("Alienigena");
    ContaPoupanca *conta_poupanca2 = new ContaPoupanca(87320,cliente4);

    // Atividade 5
    //ContaCorrente *contasCorrentes[100] = new ContaCorrente(rand() % 99999,);
    // criar um vetor simples de novas 100 contasCorrentes ira fazer uma
    // nova chamada para cada objeto instanciado, aumenta e muito o consumo de memoria
    int n = 100;
    ContaCorrente **novas_contas = new ContaCorrente*[n];

    // Atividade 6
    //Conta *contas[100] = new Conta(rand() % 99999,)

//    Cliente cliente5("Gray");
//    ContaPoupanca conta_poupanca3(73348,cliente5);

//    Cliente cliente6("Dot");
//    ContaPoupanca conta_corrente3(7316,cliente6);

    conta_corrente1->Deposita(129.00);
    conta_corrente1->Deposita(234.00);

    conta_corrente2->Deposita(9234.00);
    conta_corrente2->Retirada(4434.00);

    conta_corrente1->Extrato();
    conta_corrente2->Extrato();

    conta_poupanca1->Deposita(2245.00);
    conta_poupanca2->Deposita(385.00);

    conta_poupanca1->Retirada(305.00);
    conta_poupanca2->Retirada(100.00);

//    conta_corrente3.Deposita(100.00);
//    conta_poupanca3.Deposita(100.00);

    conta_corrente1->Transferencia(conta_corrente2,1000.00);
    conta_corrente1->Transferencia(conta_poupanca1,120.00);

    conta_corrente1->AplicacaoJurosDiarios(100);
    conta_poupanca2->AplicacaoJurosDiarios(100);

    conta_corrente1->Extrato();
    conta_poupanca1->Extrato();
    conta_poupanca2->Extrato();

     conta_corrente1->LimparExtrato();
     conta_corrente2->LimparExtrato();
//    conta_corrente3.LimparExtrato();
//    conta_poupanca1.LimparExtrato();
    conta_poupanca1->LimparExtrato();
    conta_poupanca2->LimparExtrato();

// instrução para limpar a pasta extrato

// parece perigodo fazer isso
//#ifdef _WIN32
//   system("del extratos/*.txt");
//   remove("extratos/*.txt");
//#else
//   system("exec rm -r /extrato/*");
//#endif

    return 0;
}
