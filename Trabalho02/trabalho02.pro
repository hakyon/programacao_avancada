TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    conta.cpp \
    cliente.cpp

HEADERS += \
    veiculo.h \
    conta.h \
    cliente.h
