### Universidade Tuiuti do Parana ####
### Ciência da Computação ###
### Disciplina: Programação Avançada ###

### Exercicios/Trabalhos ###

* Periodo : 01/2017 - 06/2017
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: C++
* IDE: QtCreator
* Plataforma : Linux/Windows

### Assuntos  ###
* Polimorfismo
* Sobrecarga de operador
* Design Pattern
* Ferramentas de UML em conjunto ao desenvolvimento do software
* Projeto baseado em MVC

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
