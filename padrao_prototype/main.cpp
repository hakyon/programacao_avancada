#include <iostream>

#include "documento.h"
#include "gerenciadordocumentos.h"


using namespace std;

int main() {
    vector<Documento*> docs(N); // declarando um vetor de ponteiros de documentos

    int escolha;
    cout << "sair(0), xml(1), txt(2), PDF(3): \n";

    while (true) { // enquanto nao for criado um documento de cada tipo a chamada continuara
        cout << "Faca uma escolha (0-3)\n";
        cin >> escolha;
        if (escolha <= 0 || escolha >= N)
            break;
        docs[escolha] = GerenciadorDocumentos::CriarDocumento( escolha );
    }

    for (int i = 1; i < docs.size(); ++i){
        if(docs[i]) docs[i]->armazena();
    }

    Destruct d;
    // this calls Destruct::operator()
    for_each(docs.begin(), docs.end(), d);

    return 0;
}
