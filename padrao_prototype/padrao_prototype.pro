TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    documento.cpp \
    documento_concreto.cpp \
    gerenciadordocumentos.cpp

HEADERS += \
    documento.h \
    documento_concreto.h \
    gerenciadordocumentos.h
