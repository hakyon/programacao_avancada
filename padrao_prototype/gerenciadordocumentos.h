#ifndef GERENCIADORDOCUMENTOS_H
#define GERENCIADORDOCUMENTOS_H

#include "documento_concreto.h"


// criando documentos chamando metodo clone do prototipo que herda e
// interface virtual (prototipo)

class GerenciadorDocumentos
{
public:
    GerenciadorDocumentos();
    static Documento* CriarDocumento( int escolha );

    ~GerenciadorDocumentos(){}

private:
    static Documento* Meus_tipos_doc[N]; // vetor de ponteiros do tipo Documento
};


struct Destruct  // função do tipo struct para poder desalocar na memoria os documentos
{
    void operator()(Documento *a) const {
        delete a;
    }
};

#endif // GERENCIADORDOCUMENTOS_H
