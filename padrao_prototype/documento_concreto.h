#ifndef DOCUMENTO_CONCRETO_H
#define DOCUMENTO_CONCRETO_H

#include "documento.h"

// Concrete prototypes : xmlDoc, plainDoc, spreadsheetDoc

class documento_concreto
{
public:
    documento_concreto();
};


class Xml_doc : public Documento
{
public:
    Documento*   clone() const{
        return new Xml_doc;
    }
    void armazena() const {
        cout << "Novo xml_Doc\n";
    }
};

class Txt_doc : public Documento
{
public:
    Documento* clone() const {
        return new Txt_doc;
    }
    void armazena() const {
        cout << "Novo Txt Doc\n";
    }
};

class Pdf_doc : public Documento
{
public:
    Documento* clone() const {
        return new Pdf_doc;
    }
    void armazena() const {
        cout << "Novo PDF Doc\n";
    }
};



#endif // DOCUMENTO_CONCRETO_H
