#ifndef DOCUMENTO_H
#define DOCUMENTO_H

#include "prototype.h"

#include <exception>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;
const int N = 4; // definindo limite de criacao

// Prototype

class Documento
{
public:
    Documento();
    virtual Documento* clone() const = 0;
    virtual void armazena() const = 0;
    virtual ~Documento() { }
};

#endif // DOCUMENTO_H
