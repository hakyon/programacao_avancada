#ifndef PROTOTYPE_CONCRETO_H
#define PROTOTYPE_CONCRETO_H

#include "prototype.h"


class prototype_concreto
{
public:
    prototype_concreto();
};

// Concrete prototypes : xmlDoc, plainDoc, spreadsheetDoc

//class xmlDoc : public Document
//{
//public:
//   Document*   clone() const { return new xmlDoc; }
//   void store() const { cout << "xmlDoc\n"; }
//};

//class plainDoc : public Document
//{
//public:
//   Document* clone() const { return new plainDoc; }
//   void store() const { cout << "plainDoc\n"; }
//};

//class spreadsheetDoc : public Document
//{
//public:
//   Document* clone() const { return new spreadsheetDoc; }
//   void store() const { cout << "spreadsheetDoc\n"; }
//};

#endif // PROTOTYPE_CONCRETO_H
